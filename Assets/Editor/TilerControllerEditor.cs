﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TilerController))]
public class TilerControllerEditor : Editor {
  private int selectedType;

  public override void OnInspectorGUI() {
    DrawDefaultInspector();

    TilerController tiler = (TilerController) target;
    int tileSize = EditorGUILayout.IntField("Tile Size", tiler.tileSize);

    if (GUI.changed) {
      /* tiler.setTileSize(tileSize); */
    }

    string[] types = {
      "Path",
      "Rock"
    };

    selectedType = EditorGUILayout.Popup("Type", selectedType, types);
  }

  public void OnSceneGUI() {
    int controlID = GUIUtility.GetControlID(FocusType.Passive);
    Tools.current = Tool.None;

    Ray mouseRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
    Vector2 mouseWorldPosition = new Vector2(mouseRay.origin.x, mouseRay.origin.y);

    TilerController tiler = (TilerController) target;
    switch (Event.current.type) {
      case EventType.MouseDrag:
        tiler.set(mouseWorldPosition, selectedType);
        EditorUtility.SetDirty(tiler);
        break;
      case EventType.layout:
        HandleUtility.AddDefaultControl(controlID);
        break;
    }
  }
}
