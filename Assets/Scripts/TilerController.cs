﻿using UnityEngine;
using System.Collections;

public class TilerController : MonoBehaviour {
  [HideInInspector]
  public int tileSize;
  public GameObject[] prefabs;
  private GameObject[,] grid = new GameObject[15, 10];

  void Start() {
    set(0, 0, 0);
  }

  public void set(Vector2 point, int type) {
    int c = Mathf.FloorToInt(point.x / tileSize);
    int r = Mathf.FloorToInt(point.y / tileSize);
    if (c >= 0 && r >= 0 && c < grid.GetLength(1) && r < grid.GetLength(0)) {
      set(c, r, type);
    }
  }

  public void set(int c, int r, int type) {
    if (grid[r, c] != null) {
      DestroyImmediate(grid[r, c]);
    }

    Vector3 position = new Vector3((c + 0.5f) * tileSize, (r + 0.5f) * tileSize, 0.0f);
    grid[r, c] = (GameObject) Instantiate(prefabs[type], position, Quaternion.identity);
    grid[r, c].transform.parent = transform;
    grid[r, c].transform.localScale = new Vector3(tileSize, tileSize, 1.0f);
  }
}
